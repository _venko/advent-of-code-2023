use std::collections::HashSet;

fn parse_cards(s: &str) -> impl Iterator<Item = &str> + '_ {
    s.split(|ch: char| !ch.is_numeric())
        .filter(|s| !s.is_empty())
}

fn parse_line(line: &str) -> (HashSet<&str>, impl Iterator<Item = &str> + '_) {
    let line = line.split_at(8).1;
    let mut parts = line.split(" | ");
    let winning_cards = parse_cards(parts.next().unwrap()).collect::<HashSet<&str>>();
    let held_cards_iter = parse_cards(parts.next().unwrap());
    (winning_cards, held_cards_iter)
}

fn count_winners<'a>(
    (winning_cards, held_cards_iter): (HashSet<&str>, impl Iterator<Item = &'a str> + 'a),
) -> usize {
    held_cards_iter
        .filter(|num| winning_cards.contains(num))
        .count()
}

pub fn day_4_part_a(input: String) -> usize {
    input
        .lines()
        .map(parse_line)
        .map(count_winners)
        .map(|held_winners| match held_winners as u32 {
            0 => 0,
            held_winners => usize::pow(2, held_winners - 1),
        })
        .sum()
}

pub fn day_4_part_b(input: String) -> usize {
    let mut copies = vec![1; input.lines().count()];

    input
        .lines()
        .map(parse_line)
        .map(count_winners)
        .enumerate()
        .for_each(|(card, wins)| {
            let incrememnt = copies[card];
            copies[1 + card..1 + card + wins]
                .iter_mut()
                .for_each(|count| *count += incrememnt);
        });

    copies.iter().sum()
}
