use aho_corasick::AhoCorasick;

const COMPOUND_PATTERNS: &[&str] = &[
    "oneight",
    "twone",
    "threeight",
    "fiveight",
    "sevenine",
    "eightwo",
    "eighthree",
    "nineight",
];

const COMPOUND_REPLACEMENTS: &[&str] = &["18", "21", "38", "58", "79", "82", "83", "98"];

const SIMPLE_PATTERNS: &[&str] = &[
    "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
];

const SIMPLE_REPLACEMENTS: &[&str] = &["1", "2", "3", "4", "5", "6", "7", "8", "9"];

fn extract_numbers(line: &str) -> Vec<usize> {
    line.chars()
        .filter(|ch| ch.is_numeric())
        .map(|ch| {
            ch.to_digit(10)
                .unwrap_or_else(|| panic!("Could not convert {ch} to digit")) as usize
        })
        .collect()
}

fn replace(input: &str, patterns: &[&str], replacements: &[&str]) -> String {
    let ac = AhoCorasick::new(patterns).unwrap();
    ac.replace_all(input, replacements)
}

pub fn day_1_part_a(input: String) -> usize {
    input
        .lines()
        .map(extract_numbers)
        .map(|nums| {
            (10 * nums.first().expect("Could not get first digit in line"))
                + nums.last().expect("Could not get last digit in line")
        })
        .sum()
}

pub fn day_1_part_b(input: String) -> usize {
    let converted = {
        let intermediate = replace(&input, COMPOUND_PATTERNS, COMPOUND_REPLACEMENTS);
        replace(&intermediate, SIMPLE_PATTERNS, SIMPLE_REPLACEMENTS)
    };

    // To see conversions:
    // input
    //     .split("\n")
    //     .zip(converted.split("\n"))
    //     .for_each(|(orig, conv)| println!("{orig} -> {conv}"));

    converted
        .lines()
        .map(extract_numbers)
        .map(|nums| {
            (10 * nums.first().expect("Could not get first digit in line"))
                + nums.last().expect("Could not get last digit in line")
        })
        .sum()
}
