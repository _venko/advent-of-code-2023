use std::fmt;
use std::io::{stdout, Write};
use std::path::{Path, PathBuf};
use std::time::{Duration, Instant};

use itertools::Itertools;

use crate::constants::INPUT_ROOT;

/// The day of a problem.
///
/// Advent of Code problems are orgnanized into days, with 25 in total.
pub type ProblemDay = u8;

/// Represents the "part" of a day's problem.
///
/// Advent of Code problems are two parts, with the first part being denoted
/// here as `A` and the second as `B`.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum ProblemPart {
    A,
    B,
}

impl fmt::Display for ProblemPart {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ProblemPart::A => write!(f, "A"),
            ProblemPart::B => write!(f, "B"),
        }
    }
}

/// Indicates whether a solution's input is the smaller test case provided with
/// each problem or the full sized, "real" input.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum InputCase {
    Test,
    Real,
}

impl fmt::Display for InputCase {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            InputCase::Test => write!(f, "Test"),
            InputCase::Real => write!(f, "Real"),
        }
    }
}

/// A result of attempting to run a solution on either its test or real input.
type SolutionResult = Result<usize, Box<dyn std::error::Error + 'static>>;

/// The output of running a `Solution` against its inputs.
struct Output {
    day: ProblemDay,
    part: ProblemPart,
    case: InputCase,
    result: SolutionResult,
    elapsed: Duration,
}

impl std::fmt::Display for Output {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "Problem {} part {}", self.day, self.part)?;
        write!(
            f,
            "  {}: {} ({:#?})",
            self.case,
            self.result
                .as_ref()
                .map_or_else(|v| v.to_string(), |e| e.to_string()),
            self.elapsed
        )
    }
}

/// Holds information about an Advent of Code problem.
struct Solution {
    day: ProblemDay,
    part: ProblemPart,
    case: InputCase,
    /// The solver function used to produce an answer for the problem.
    solver: fn(String) -> usize,
    input: PathBuf,
}

impl Solution {
    fn get_result(&self) -> SolutionResult {
        let input = std::fs::read_to_string(self.input.clone())?;
        Ok((self.solver)(input))
    }

    pub fn run(&self) -> Output {
        let now = Instant::now();
        let result = self.get_result();
        let elapsed = now.elapsed();
        Output {
            day: self.day,
            part: self.part,
            case: self.case,
            result,
            elapsed,
        }
    }
}

/// Handles the registration and exection of Advent of Code inputs and solver functions.
pub struct Runner {
    solutions: Vec<Solution>,
}

impl Runner {
    pub fn new() -> Runner {
        Runner { solutions: vec![] }
    }

    /// Adds a new solution to this runner.
    fn register_solution(
        &mut self,
        day: ProblemDay,
        part: ProblemPart,
        case: InputCase,
        solver: fn(String) -> usize,
        input: &str,
    ) -> &mut Runner {
        let input = Path::new(INPUT_ROOT).join(input);
        self.solutions.push(Solution {
            day,
            part,
            case,
            solver,
            input,
        });
        self
    }

    /// Adds a new solution targeting a test input to this runner.
    pub fn add_test_case(
        &mut self,
        day: ProblemDay,
        part: ProblemPart,
        solver: fn(String) -> usize,
        input: &str,
    ) -> &mut Runner {
        self.register_solution(day, part, InputCase::Test, solver, input)
    }

    pub fn add_real_case(
        &mut self,
        day: ProblemDay,
        part: ProblemPart,
        solver: fn(String) -> usize,
        input: &str,
    ) -> &mut Runner {
        self.register_solution(day, part, InputCase::Real, solver, input)
    }

    /// Execute all solutions managed by this runner.
    pub fn execute(&self) {
        print!("Running");
        let outputs: Vec<Output> = self
            .solutions
            .iter()
            .inspect(|_| {
                print!(".");
                stdout().flush().unwrap();
            })
            .map(Solution::run)
            .collect();
        println!("done.");

        for (day, group) in &outputs.into_iter().group_by(|o| o.day) {
            println!("Day: {day}");
            for output in group {
                println!(
                    "  {} {} {:<10} {:#?}",
                    output.part,
                    output.case,
                    output
                        .result
                        .as_ref()
                        .map_or_else(|v| v.to_string(), |e| e.to_string()),
                    output.elapsed
                );
            }
        }
    }
}
