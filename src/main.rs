//#![allow(unused)]
extern crate aho_corasick;
extern crate num;
extern crate rayon;

mod constants;
mod runner;
mod solutions;

use runner::{ProblemPart, Runner};
use solutions::*;

fn main() {
    let mut runner = Runner::new();
    runner
        .add_test_case(1, ProblemPart::A, day_1_part_a, "1a_test.txt")
        .add_real_case(1, ProblemPart::A, day_1_part_a, "1_real.txt")
        .add_test_case(1, ProblemPart::B, day_1_part_b, "1b_test.txt")
        .add_test_case(1, ProblemPart::B, day_1_part_b, "1_real.txt")
        .add_test_case(2, ProblemPart::A, day_2_part_a, "2a_test.txt")
        .add_real_case(2, ProblemPart::A, day_2_part_a, "2_real.txt")
        .add_test_case(2, ProblemPart::B, day_2_part_b, "2a_test.txt")
        .add_test_case(2, ProblemPart::B, day_2_part_b, "2_real.txt")
        .add_test_case(3, ProblemPart::A, day_3_part_a, "3a_test.txt")
        .add_real_case(3, ProblemPart::A, day_3_part_a, "3_real.txt")
        .add_test_case(4, ProblemPart::A, day_4_part_a, "4a_test.txt")
        .add_real_case(4, ProblemPart::A, day_4_part_a, "4_real.txt")
        .add_test_case(4, ProblemPart::B, day_4_part_b, "4a_test.txt")
        .add_test_case(4, ProblemPart::B, day_4_part_b, "4_real.txt")
        .add_test_case(5, ProblemPart::A, day_5_part_a, "5a_test.txt")
        .add_real_case(5, ProblemPart::A, day_5_part_a, "5_real.txt")
        .add_test_case(5, ProblemPart::B, day_5_part_b, "5a_test.txt")
        .add_real_case(5, ProblemPart::B, day_5_part_b, "5_real.txt")
        .add_test_case(7, ProblemPart::A, day_7_part_a, "7a_test.txt")
        .add_real_case(7, ProblemPart::A, day_7_part_a, "7_real.txt")
        .add_test_case(8, ProblemPart::A, day_8_part_a, "8a_test.txt")
        .add_test_case(8, ProblemPart::A, day_8_part_a, "8a_test2.txt")
        .add_real_case(8, ProblemPart::A, day_8_part_a, "8_real.txt")
        .add_test_case(8, ProblemPart::B, day_8_part_b, "8b_test.txt")
        .add_test_case(8, ProblemPart::B, day_8_part_b, "8_real.txt")
        .execute();
}
