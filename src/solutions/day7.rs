use std::cmp::Ordering;

use itertools::*;

type Bid = usize;

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
enum Card {
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    T,
    J,
    Q,
    K,
    A,
}

impl From<char> for Card {
    fn from(ch: char) -> Self {
        use Card::*;
        match ch {
            '2' => Two,
            '3' => Three,
            '4' => Four,
            '5' => Five,
            '6' => Six,
            '7' => Seven,
            '8' => Eight,
            '9' => Nine,
            'T' => T,
            'J' => J,
            'Q' => Q,
            'K' => K,
            'A' => A,
            _ => unreachable!("Unparseable card"),
        }
    }
}

struct SortedCards(Vec<Card>);

impl From<Vec<Card>> for SortedCards {
    fn from(mut hand: Vec<Card>) -> SortedCards {
        hand.sort();
        SortedCards(hand)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum Hand {
    CardHigh,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

impl Hand {
    fn try_five_of_a_kind(cards: &SortedCards) -> Option<Hand> {
        match &cards.0[..] {
            [c0, c1, c2, c3, c4] if c0 == c1 && c1 == c2 && c2 == c3 && c3 == c4 => {
                Some(Hand::FiveOfAKind)
            }
            _ => None,
        }
    }

    fn try_four_of_a_kind(cards: &SortedCards) -> Option<Hand> {
        match &cards.0[..] {
            [c0, c1, c2, c3, _] if c0 == c1 && c1 == c2 && c2 == c3 => Some(Hand::FourOfAKind),
            [_, c1, c2, c3, c4] if c1 == c2 && c2 == c3 && c3 == c4 => Some(Hand::FourOfAKind),
            _ => None,
        }
    }

    fn try_full_house(cards: &SortedCards) -> Option<Hand> {
        match &cards.0[..] {
            [c0, c1, c2, c3, c4] if (c0 == c1 && c1 == c2) && (c3 == c4) => Some(Hand::FullHouse),
            [c0, c1, c2, c3, c4] if (c0 == c1) && (c2 == c3 && c3 == c4) => Some(Hand::FullHouse),
            _ => None,
        }
    }

    fn try_three_of_a_kind(cards: &SortedCards) -> Option<Hand> {
        match &cards.0[..] {
            [c0, c1, c2, _, _] if c0 == c1 && c1 == c2 => Some(Hand::ThreeOfAKind),
            [_, c1, c2, c3, _] if c1 == c2 && c2 == c3 => Some(Hand::ThreeOfAKind),
            [_, _, c2, c3, c4] if c2 == c3 && c3 == c4 => Some(Hand::ThreeOfAKind),
            _ => None,
        }
    }

    fn try_two_pair(cards: &SortedCards) -> Option<Hand> {
        match &cards.0[..] {
            [c0, c1, c2, c3, _] if c0 == c1 && c2 == c3 => Some(Hand::TwoPair),
            [c0, c1, _, c3, c4] if c0 == c1 && c3 == c4 => Some(Hand::TwoPair),
            [_, c1, c2, c3, c4] if c1 == c2 && c3 == c4 => Some(Hand::TwoPair),
            _ => None,
        }
    }

    fn try_one_pair(cards: &SortedCards) -> Option<Hand> {
        match &cards.0[..] {
            [c0, c1, _, _, _] if c0 == c1 => Some(Hand::OnePair),
            [_, c1, c2, _, _] if c1 == c2 => Some(Hand::OnePair),
            [_, _, c2, c3, _] if c2 == c3 => Some(Hand::OnePair),
            [_, _, _, c3, c4] if c3 == c4 => Some(Hand::OnePair),
            _ => None,
        }
    }
}

impl From<SortedCards> for Hand {
    fn from(cards: SortedCards) -> Self {
        Hand::try_five_of_a_kind(&cards)
            .or_else(|| Hand::try_four_of_a_kind(&cards))
            .or_else(|| Hand::try_full_house(&cards))
            .or_else(|| Hand::try_three_of_a_kind(&cards))
            .or_else(|| Hand::try_two_pair(&cards))
            .or_else(|| Hand::try_one_pair(&cards))
            .unwrap_or(Hand::CardHigh)
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Round {
    cards: Vec<Card>,
    hand: Hand,
    bid: Bid,
}

impl PartialOrd for Round {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Round {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.hand.cmp(&other.hand) {
            Ordering::Equal => self.cards.cmp(&other.cards),
            ord => ord,
        }
    }
}

impl From<&str> for Round {
    fn from(line: &str) -> Self {
        let mut parts = line.split_whitespace();

        let card_str = parts.next().unwrap();
        let cards = card_str.chars().map(Card::from).collect_vec();

        let hand = Hand::from(SortedCards::from(cards.clone()));

        let bid_str = parts.next().unwrap();
        let bid = bid_str.parse::<usize>().unwrap();

        Round { cards, hand, bid }
    }
}

pub fn day_7_part_a(input: String) -> usize {
    input
        .lines()
        .map(Round::from)
        .sorted()
        .enumerate()
        .map(|(i, round)| (i + 1, round))
        .map(|(rank, round)| rank * round.bid)
        .sum()
}
