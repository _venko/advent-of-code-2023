use std::ops::Range;

use itertools::Itertools;

trait Contains {
    fn contains(&self, i: usize) -> bool;
}

impl Contains for Range<usize> {
    fn contains(&self, i: usize) -> bool {
        self.start <= i && i < self.end
    }
}

#[derive(Debug, Clone)]
struct Span {
    src_start: usize,
    dst_start: usize,
    len: usize,
}

impl Span {
    fn contains(&self, i: usize) -> bool {
        self.src_start <= i && i < self.src_start + self.len
    }

    fn map(&self, i: usize) -> usize {
        match i {
            index if self.contains(index) => self.dst_start + index - self.src_start,
            _ => i,
        }
    }

    fn invert(&self) -> InvSpan {
        InvSpan(self.clone())
    }
}

struct InvSpan(Span);

impl InvSpan {
    fn contains(&self, i: usize) -> bool {
        self.0.dst_start <= i && i < self.0.dst_start + self.0.len
    }

    fn map(&self, i: usize) -> usize {
        match i {
            index if self.contains(index) => self.0.src_start + index - self.0.dst_start,
            _ => i,
        }
    }
}

#[derive(Clone)]
struct Map(Vec<Span>);

impl Map {
    fn map(&self, i: usize) -> usize {
        if let Some(covering_span) = self.0.iter().find(|s| s.contains(i)) {
            covering_span.map(i)
        } else {
            i
        }
    }

    fn invert(&self) -> InvMap {
        InvMap(self.0.iter().map(Span::invert).collect_vec())
    }
}

struct InvMap(Vec<InvSpan>);

impl InvMap {
    fn map(&self, i: usize) -> usize {
        if let Some(covering_span) = self.0.iter().find(|s| s.contains(i)) {
            covering_span.map(i)
        } else {
            i
        }
    }
}

fn parse_nums(s: &str) -> impl Iterator<Item = usize> + '_ {
    s.split(|ch: char| !ch.is_numeric())
        .filter(|s| !s.is_empty())
        .map(|s| s.parse().unwrap())
}

fn parse_map(block: &[&str]) -> Map {
    let mut iter = block.iter();
    iter.next(); // Drop name.
    Map(iter
        .map(|line| {
            let mut nums = parse_nums(line);
            let dst_start = nums.next().unwrap();
            let src_start = nums.next().unwrap();
            let len = nums.next().unwrap();
            Span {
                src_start,
                dst_start,
                len,
            }
        })
        .collect())
}

pub fn day_5_part_a(input: String) -> usize {
    let lines = input.lines().collect_vec();
    let mut block_iter = lines.split(|line| line.is_empty());

    let seeds = parse_nums(block_iter.next().and_then(|slice| slice.first()).unwrap());
    let maps = block_iter.map(parse_map).collect_vec();

    seeds
        .map(|seed| maps.iter().fold(seed, |n, m| m.map(n)))
        .min()
        .unwrap()
}

pub fn day_5_part_b(input: String) -> usize {
    let lines: Vec<&str> = input.lines().collect();
    let mut block_iter = lines.split(|line| line.is_empty());

    let seed_ranges: Vec<Range<usize>> = {
        let first_line = block_iter.next().and_then(|lines| lines.first()).unwrap();
        let temp = parse_nums(first_line).collect_vec();
        temp.chunks_exact(2)
            .map(|slice| match slice {
                &[start, len] => start..start + len,
                _ => unreachable!(),
            })
            .collect()
    };

    let maps = block_iter.map(parse_map).collect_vec();

    let mut seed_candidates = vec![0, usize::MAX];
    for inv_map in maps.iter().rev().map(Map::invert) {
        seed_candidates = seed_candidates
            .into_iter()
            .map(|candidate| inv_map.map(candidate))
            .collect_vec();
        for span in inv_map.0.iter() {
            let start = span.0.src_start;
            if start > 0 {
                seed_candidates.push(start - 1);
            }
            seed_candidates.push(start);
        }
    }

    seed_candidates
        .into_iter()
        .sorted()
        .dedup()
        .filter(|&candidate| seed_ranges.iter().any(|r| r.contains(&candidate)))
        .map(|seed| maps.iter().fold(seed, |n, m| m.map(n)))
        .min()
        .unwrap()
}
