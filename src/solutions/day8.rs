use std::{collections::HashMap, ops::Index};

use itertools::Itertools;
use num::Integer;

const START: &str = "AAA";
const END: &str = "ZZZ";

#[derive(Debug, Clone, Copy)]
enum Direction {
    Left = 0,
    Right = 1,
}

impl From<char> for Direction {
    fn from(value: char) -> Self {
        match value {
            'L' => Direction::Left,
            'R' => Direction::Right,
            _ => unreachable!(),
        }
    }
}

struct Node<'a>([&'a str; 2]);

impl<'a> Index<Direction> for Node<'a> {
    type Output = str;

    fn index(&self, index: Direction) -> &Self::Output {
        self.0[index as usize]
    }
}

impl<'a> Node<'a> {
    fn new(left: &'a str, right: &'a str) -> Node<'a> {
        Node([left, right])
    }
}

#[derive(Default)]
struct Graph<'a>(HashMap<&'a str, Node<'a>>);

impl<'a> From<&'a str> for Graph<'a> {
    fn from(value: &'a str) -> Self {
        let mut map = HashMap::with_capacity(value.lines().count());
        for line in value.lines() {
            let node_name = parse_graph_name(line);
            let left_node = parse_graph_left(line);
            let right_node = parse_graph_right(line);
            map.insert(node_name, Node::new(left_node, right_node));
        }
        Graph(map)
    }
}

impl<'a> Index<&'a str> for Graph<'a> {
    type Output = Node<'a>;

    fn index(&self, index: &'a str) -> &Self::Output {
        &self.0[index]
    }
}

#[inline(always)]
fn parse_graph_name(line: &str) -> &str {
    &line[0..3]
}

#[inline(always)]
fn parse_graph_left(line: &str) -> &str {
    &line[7..10]
}

#[inline(always)]
fn parse_graph_right(line: &str) -> &str {
    &line[12..15]
}

pub fn day_8_part_a(input: String) -> usize {
    let (moves_str, map_str) = {
        let mut blocks = input.split("\n\n");
        (blocks.next().unwrap(), blocks.next().unwrap())
    };

    let graph = Graph::from(map_str);

    moves_str
        .chars()
        .cycle()
        .map(Direction::from)
        .scan(START, |current_node, direction| {
            (*current_node != END).then(|| *current_node = &graph[current_node][direction])
        })
        .count()
}

pub fn day_8_part_b(input: String) -> usize {
    let (moves_str, map_str) = {
        let mut blocks = input.split("\n\n");
        (blocks.next().unwrap(), blocks.next().unwrap())
    };

    let graph = Graph::from(map_str);

    map_str
        .lines()
        .map(parse_graph_name)
        .filter(|name| name.chars().nth(2).unwrap() == 'A')
        .map(|node| {
            moves_str
                .chars()
                .cycle()
                .map(Direction::from)
                .scan(node, |current_node, direction| {
                    (current_node.chars().nth(2).unwrap() != 'Z')
                        .then(|| *current_node = &graph[current_node][direction])
                })
                .count()
        })
        .reduce(|acc, n| acc.lcm(&n))
        .unwrap()
}
