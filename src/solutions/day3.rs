/// BROKEN

#[allow(unused)]
struct Shape {
    rows: usize,
    cols: usize,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Coord {
    row: isize,
    col: isize,
}

impl From<(usize, usize)> for Coord {
    fn from(value: (usize, usize)) -> Self {
        Coord {
            row: value.0 as isize,
            col: value.1 as isize,
        }
    }
}

trait BBox {
    fn left(&self) -> isize;
    fn right(&self) -> isize;
    fn top(&self) -> isize;
    fn bottom(&self) -> isize;
    fn contains(&self, point: &Coord) -> bool {
        self.left() <= point.col
            && point.col <= self.right()
            && self.top() <= point.row
            && point.row <= self.bottom()
    }
}

#[derive(Debug, Clone)]
struct Symbol {
    #[allow(unused)]
    raw: char,
    coord: Coord,
}

/// A number found within the problem input.
#[derive(Debug, Clone)]
struct Number {
    raw: String,
    coord: Coord,
    len: usize,
}

impl BBox for Number {
    fn left(&self) -> isize {
        self.coord.col - 1
    }
    fn right(&self) -> isize {
        self.coord.col + self.len as isize
    }
    fn top(&self) -> isize {
        self.coord.row - 1
    }
    fn bottom(&self) -> isize {
        self.coord.row + 1
    }
}

impl Number {
    pub fn new<T: Into<Coord>>(raw: String, coord: T, len: usize) -> Number {
        Number {
            raw,
            coord: coord.into(),
            len,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
enum ParseMode {
    Skip,
    Take,
}
fn parse_nums_from(row: usize, line: &str) -> Vec<Number> {
    fn make_number(row: usize, start_col: usize, end_col: usize, line: &str) -> Number {
        let raw = line[start_col..end_col].chars().collect();
        let len = end_col - start_col;
        Number::new(raw, (row, start_col), len)
    }

    use ParseMode::*;
    let mut nums: Vec<Number> = vec![];
    let mut mode = Skip; // Arbitrary; set on first iteration.
    let mut start_col = 0;
    for (current_col, ch) in line.char_indices() {
        if current_col == 0 {
            mode = if ch.is_numeric() { Take } else { Skip };
            continue;
        }

        match (mode, ch.is_numeric()) {
            (Take, false) => {
                let end_col = current_col;
                nums.push(make_number(row, start_col, end_col, line));
                mode = Skip;
            }
            (Skip, true) => {
                start_col = current_col;
                mode = Take;
            }
            (Skip, false) | (Take, true) => (),
        }
    }

    if mode == Take {
        let end_col = line.len() - 1;
        nums.push(make_number(row, start_col, end_col, line));
    }

    nums
}

fn parse_nums(lines: &[&str]) -> Vec<Number> {
    lines
        .iter()
        .enumerate()
        .flat_map(|(row, line)| parse_nums_from(row, line))
        .collect()
}

fn parse_syms(lines: &[&str]) -> Vec<Symbol> {
    lines
        .iter()
        .enumerate()
        .flat_map(|(row, line)| {
            line.char_indices().filter_map(move |(col, ch)| {
                if !ch.is_numeric() && ch != '.' {
                    Some(Symbol {
                        raw: ch,
                        coord: (row, col).into(),
                    })
                } else {
                    None
                }
            })
        })
        .collect()
}

pub fn day_3_part_a(input: String) -> usize {
    let lines: Vec<&str> = input.lines().collect();
    let nums = parse_nums(&lines);
    let syms = parse_syms(&lines);
    nums.into_iter()
        .map(|num| {
            if syms.iter().any(|sym| num.contains(&sym.coord)) {
                num.raw.parse().unwrap()
            } else {
                0
            }
        })
        .sum::<usize>()
}
