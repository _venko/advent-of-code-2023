fn parse_line((line_num, line): (usize, &str)) -> (usize, &str) {
    let parts: Vec<&str> = line.split(':').collect();
    let preamble = parts[0];
    (
        preamble[5..]
            .parse()
            .unwrap_or_else(|_| panic!("Could not extract game num on line {line_num}")),
        parts[1],
    )
}

fn color_to_index(color: &str) -> usize {
    match color {
        "red" => 0,
        "green" => 1,
        "blue" => 2,
        _ => panic!("Unrecognized color"),
    }
}

fn game_maxes(record: &str) -> [usize; 3] {
    let mut maxes = [0, 0, 0]; // red, green, blue
    for round in record.trim().split(';') {
        let mut counts = [0, 0, 0]; // red, green, blue
        for play in round.split(',').map(str::trim) {
            let parts: Vec<&str> = play.split(' ').collect();
            let count: usize = parts[0]
                .parse()
                .unwrap_or_else(|_| panic!("Could not parse count from play {play}"));
            let color_key: usize = color_to_index(parts[1]);
            counts[color_key] += count;
        }
        for color_key in 0..3 {
            maxes[color_key] = usize::max(maxes[color_key], counts[color_key]);
        }
    }
    maxes
}

pub fn day_2_part_a(input: String) -> usize {
    let color_maxes = [12, 13, 14]; // red, green, blue

    input
        .lines()
        .enumerate()
        .map(parse_line)
        .map(|(game_num, record)| (game_num, game_maxes(record)))
        .filter(|(_, maxes)| {
            maxes
                .iter()
                .zip(color_maxes.iter())
                .all(|(count, max)| count <= max)
        })
        .map(|(game_num, _)| game_num)
        .sum()
}

pub fn day_2_part_b(input: String) -> usize {
    input
        .lines()
        .enumerate()
        .map(parse_line)
        .map(|(_, record)| game_maxes(record))
        .map(|[red, green, blue]| red * green * blue)
        .sum()
}
